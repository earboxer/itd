module go.arsenm.dev/itd

go 1.16

require (
	fyne.io/fyne/v2 v2.1.2
	github.com/VividCortex/ewma v1.2.0 // indirect
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/gen2brain/dlgs v0.0.0-20211108104213-bade24837f0b
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20211204153444-caad923f49f4 // indirect
	github.com/godbus/dbus/v5 v5.0.6
	github.com/google/uuid v1.3.0
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/knadh/koanf v1.4.0
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mitchellh/mapstructure v1.4.3
	github.com/mozillazg/go-pinyin v0.19.0
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/rs/zerolog v1.26.0
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/srwiley/oksvg v0.0.0-20211120171407-1837d6608d8c // indirect
	github.com/srwiley/rasterx v0.0.0-20210519020934-456a8d69b780 // indirect
	github.com/urfave/cli/v2 v2.3.0
	github.com/yuin/goldmark v1.4.4 // indirect
	go.arsenm.dev/infinitime v0.0.0-20220304200437-7026da3f6f14
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/net v0.0.0-20211209124913-491a49abca63 // indirect
	golang.org/x/text v0.3.7
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
